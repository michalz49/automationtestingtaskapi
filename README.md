Automation testing task #2 (API)

Resource: https://swapi.dev/

Endpoints examples:
• find in site tutorial

Tools: Java AND Rest Assured

Use Case:
1. Search for a person with the name Vader.
2. Using previous response (1) find which film that Darth Vader joined has the less planets.
3. Using previous responses (1) & (2) verify if Vader's starship is on film from response (2).
4. Find the oldest person ever played in all Star Wars films with less than 10 requests.
5. Create contract (Json schema validation) test for /people API.
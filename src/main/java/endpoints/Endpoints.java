package endpoints;

public class Endpoints {

    public static final String BASE_URL = "https://swapi.dev/api/";

    public static final String PEOPLE_ENDPOINT = BASE_URL + "people/";
}

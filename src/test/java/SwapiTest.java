import endpoints.Endpoints;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import models.Film;
import models.Person;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.*;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class SwapiTest {

    @Test
    void vaderTest() {
        // 1
        Response searchVaderResponse =
                given().baseUri(Endpoints.PEOPLE_ENDPOINT).queryParam("search", "Vader")
                        .when().get()
                        .then().statusCode(200).extract().response();
        Person vader = searchVaderResponse.jsonPath().getObject("results[0]", Person.class);
        System.out.println("1. Person with the name Vader: " + vader);

        // 2
        List<String> vaderFilmsIds = vader.getFilms();

        List<Film> vaderFilms = new ArrayList<>();
        vaderFilmsIds.forEach(film ->
                vaderFilms.add(given().baseUri(film)
                        .when().get()
                        .then().statusCode(200).extract().response().as(Film.class)));

        Map<String, Integer> filmAndNumberOfPlanets = new HashMap<>();
        vaderFilms.forEach(film -> filmAndNumberOfPlanets.put(film.getTitle(), film.getPlanets().size()));

        String titleOfFilmWithTheLessPlanets = filmAndNumberOfPlanets.entrySet().stream()
                .min(Comparator.comparingInt(Map.Entry::getValue))
                .orElseThrow(() -> new RuntimeException("Could not find the film with the less planets"))
                .getKey();
        Film filmWithTheLessPlanets = vaderFilms.stream()
                .filter(film -> film.getTitle().equals(titleOfFilmWithTheLessPlanets))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Could not get full response of the film with the less planets"));
        System.out.println("2. Film with the less planets: " + filmWithTheLessPlanets);


        // 3
        String vaderStarship = vader.getStarships().get(0);
        List<String> filmStarships = filmWithTheLessPlanets.getStarships();
        System.out.println(
                "3. Is Vader's starships on film with the less planets: " + filmStarships.contains(vaderStarship));
        assertThat(filmStarships).contains(vaderStarship);
    }

    // 4
    @Test
    void findTheOldestPerson() {
        Response firstResponse = given().baseUri(Endpoints.PEOPLE_ENDPOINT)
                .when().get()
                .then().statusCode(200).extract().response();

        List<Person> allPeople = new ArrayList<>(firstResponse.jsonPath().getList("results", Person.class));

        String nextPageUrl = firstResponse.jsonPath().getString("next");
        String nextPageNumber = StringUtils.substringAfter(nextPageUrl, "?page=");

        int requestCounter = 1;

        while (nextPageUrl != null) {
            Response response = given().baseUri(nextPageUrl).queryParam("page", nextPageNumber)
                    .when().get()
                    .then().statusCode(200).extract().response();
            allPeople.addAll(response.jsonPath().getList("results", Person.class));

            nextPageUrl = response.jsonPath().getString("next");
            nextPageNumber = StringUtils.substringAfter(nextPageUrl, "?page=");
            requestCounter++;
        }

        List<Person> peopleWithKnownBirthYear = allPeople.stream()
                .filter(person -> !person.getBirth_year().equals("unknown"))
                .toList();

        Map<String, Float> personNameAndBirthYear = new HashMap<>();
        peopleWithKnownBirthYear.forEach(
                person -> personNameAndBirthYear.put(
                        person.getName(),
                        Float.valueOf(person.getBirth_year().replace("BBY", ""))));

        String oldestPersonName = personNameAndBirthYear.entrySet().stream()
                .max(Map.Entry.comparingByValue())
                .orElseThrow(() -> new RuntimeException("Could not find the oldest person's name"))
                .getKey();
        Person oldestPerson = peopleWithKnownBirthYear.stream()
                .filter(person -> person.getName().equals(oldestPersonName))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Could not find the oldest person"));

        System.out.println("4. Oldest person: " + oldestPerson);
        System.out.println("   Number of requests: " + requestCounter);
    }

    // 5
    @Test
    void getPeopleContractTest() {
        InputStream getPeopleJsonSchema =
                Objects.requireNonNull(
                        getClass().getClassLoader().getResourceAsStream("getPeopleSchema.json"));

        given().baseUri(Endpoints.PEOPLE_ENDPOINT)
                .when().get()
                .then().statusCode(200)
                .and()
                .assertThat().body(JsonSchemaValidator.matchesJsonSchema(getPeopleJsonSchema));
    }

    @Test
    void getSpecificPeopleContractTest() {
        InputStream getSpecificPeopleJsonSchema =
                Objects.requireNonNull(
                        getClass().getClassLoader().getResourceAsStream("getSpecificPeopleSchema.json"));

        given().baseUri(Endpoints.PEOPLE_ENDPOINT + "1")
                .when().get()
                .then().statusCode(200)
                .and()
                .assertThat().body(JsonSchemaValidator.matchesJsonSchema(getSpecificPeopleJsonSchema));
    }
}
